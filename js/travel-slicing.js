//navigation bar open/close
var hamMenu = document.getElementById('hamMenu');
var closeNav = document.getElementById('closeNav');
var sideNAv = document.getElementById('sideNav');

sideNAv.classList.add('side-nav-closed');

hamMenu.onclick = function(){
    sideNAv.classList.remove('side-nav-closed');
}
closeNav.onclick = function(){
    sideNAv.classList.add('side-nav-closed');
}

// appear on scroll by intersection Observer
var slideTargets = document.querySelectorAll(".slide-target");

const sliderOptions = { }

let slideObserver = new IntersectionObserver(function(entries, slideObserver){

    entries.forEach(entry => {
        if(!entry.isIntersecting){
            return;
        }

        console.log(entry.target);
        entry.target.classList.remove('slide-appear');
        
        slideObserver.unobserve(entry.target);
    });
    
}, sliderOptions);

slideTargets.forEach(slideTarget => {
    slideObserver.observe(slideTarget);
});



//lazy-load by intersection observer
var lazyImgs = document.querySelectorAll("[data-src]");

const lazyImgOptions = { }

const lazyImgObserver = new IntersectionObserver(function(entries, lazyImgObserver){
    entries.forEach(entry => {
        let imgSrc = entry.target.getAttribute('data-src');

        if(!entry.isIntersecting){
            return;
        }

        entry.target.src = imgSrc;
        lazyImgObserver.unobserve(entry.target);
    });
}, lazyImgOptions);

lazyImgs.forEach(lazyImg => {
    lazyImgObserver.observe(lazyImg);
});

//slide animation on sec. pages
var slideItems = document.querySelectorAll('.slide-items');
const slideItemsOptions = { }

const slideItemsObserver = new IntersectionObserver(function(entries, slideItemsObserver){
    entries.forEach(entry =>  {
        if(!entry.isIntersecting){
            return;
        }
        entry.target.classList.add('slide-original');
        slideItemsObserver.unobserve(entry.target);
    });
}, slideItemsOptions);

slideItems.forEach(slideItem => {
    slideItemsObserver.observe(slideItem);
});
